#!/bin/sh
mkdir -p nucleoid-ide/source/opt/nucleoid/
git clone https://gitlab.com/nucleoid/ide.git nucleoid-ide/source/opt/nucleoid/ide/
npm install --prefix nucleoid-ide/source/opt/nucleoid/ide/
rm -Rf nucleoid-ide/source/opt/nucleoid/ide/.git/

mkdir -p nucleoid-ide/debian
mv nucleoid-ide/source/opt/nucleoid/ide/scripts/debian/* nucleoid-ide/debian/
cp nucleoid-ide/source/opt/nucleoid/ide/LICENSE nucleoid-ide/debian/

mkdir -p nucleoid-ide/source/etc/nucleoid/
echo '{ "ide": "/opt/nucleoid/ide/ide.js" }' > nucleoid-ide/source/etc/nucleoid/config.json
