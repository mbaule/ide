$(function () {
  let history = [];
  let select = 0;
  let current = "";

  function addHistory(text) {
    if (history.length > 5) {
      history.shift();
    }

    history.push(text);

    $("#history-list").empty();

    for (let i = 0; i < history.length; i++) {
      let item = history[i];
      let div = $("<div>", {
        id: `history-item-${i}`,
        class: "history-item alert alert-secondary",
        style: `font-weight: ${i === history.length - 1 ? "500" : "normal"}`,
      }).text(item);
      $("#history-list").append(div);
    }

    let div = $("<div>", {
      class: "text-center",
      style: "margin-bottom: 8px; color: gray",
    }).html("Ctrl + &uarr;&darr;");
    $("#history-list").append(div);
  }

  var editor = ace.edit("editor");
  var keys = {};

  ace.require("ace/ext/language_tools ");

  editor.setTheme("ace/theme/chrome");
  editor.getSession().setMode("ace/mode/javascript");

  editor.setOptions({
    enableBasicAutocompletion: true,
  });
  editor.setFontSize("15px");
  editor.renderer.setScrollMargin(5);

  editor.focus();

  editor.commands.addCommand({
    name: "History-Up",
    bindKey: "Ctrl+Up",
    exec: function () {
      if (select == 0) {
        current = editor.getValue();
      }

      let length = history.length;

      if (select >= length) {
        return;
      }

      $(`#history-item-${length - select}`).removeClass("alert-dark");

      select++;

      $(`#history-item-${length - select}`).addClass("alert-dark");
      editor.setValue(history[length - select], 1);
    },
  });

  editor.commands.addCommand({
    name: "History-Down",
    bindKey: "Ctrl+Down",
    exec: function () {
      if (select <= 0) {
        return;
      }

      let length = history.length;
      $(`#history-item-${length - select}`).removeClass("alert-dark");

      select--;

      if (select == 0) {
        editor.setValue(current, 1);
      } else {
        $(`#history-item-${length - select}`).addClass("alert-dark");
        editor.setValue(history[length - select], 1);
      }
    },
  });

  editor.getSession().on("change", function () {
    $("#tick").hide();
  });

  $("#editor")
    .keydown(function (e) {
      keys[e.keyCode] = true;
    })
    .keyup(function (e) {
      empty: if (keys[13] && keys[17]) {
        $("#result").empty();
        var entry = editor.getValue().trim();
        editor.setValue("", 1);
        select = 0;
        current = "";

        if (!entry || entry == "" || entry == null || entry == undefined)
          break empty;

        addHistory(entry);
        $.ajax({
          url: "/",
          type: "post",
          contentType: "application/javascript",
          data: entry,
          headers: {
            Process: process,
          },
          success: function (response) {
            displayJSON(response);
          },
        })
          .fail(function (response) {
            if (response.readyState !== 4) {
              displayMessage("Connection lost");
              return;
            }

            if (response.status >= 500) {
              displayMessage(`Error: ${response.responseText}`);
            } else {
              displayMessage(response.responseJSON);
            }
          })
          .always(function (p1, p2, p3) {
            if (p1.status === 400 || p3 instanceof Error) {
              displayTime(p1.getResponseHeader("Server-Timing"));
            } else {
              displayTime(p3.getResponseHeader("Server-Timing"));
            }
          });
      }

      keys[e.keyCode] = false;
    });
});
