var process = "main";

$(function () {
  function getProcess(node) {
    let parents = node.parents;

    if (node.parents.length > 1) {
      let process = "";

      for (let i = parents.length - 2; i >= 0; i--) {
        let n = $("#tree").jstree(true).get_node(parents[i]);

        if (!n) {
          throw "Invalid structure of process tree";
        }

        if (n.type === "folder" || n.type === "file") {
          if (process !== "") {
            process += "/";
          }

          process += n.text;
        }
      }

      if (node.type === "folder" || node.type === "file") {
        process += "/" + node.text;
      }

      return process;
    } else {
      return node.text;
    }
  }

  $("#tree").jstree({
    plugins: ["types"],
    core: {
      check_callback: true,
    },
    types: {
      folder: {
        icon: "/images/folder.png",
      },
      file: {
        icon: "/images/leaf.png",
      },
      class: {
        icon: "/images/class.png",
      },
      instance: {
        icon: "/images/instance.png",
      },
    },
  });

  $("#tree").on("select_node.jstree", function (event, data) {
    process = getProcess(data.node);

    if (data.node.type === "class" || data.node.type === "instance") {
      let parents = data.node.parents;

      $.ajax({
        url: "/",
        type: "post",
        contentType: "application/javascript",
        data: `graph('${data.node.text}')`,
        headers: {
          Process: getProcess(data.node),
        },
        success: function (json) {
          displayGraph(data.node.text, json);
        },
      });
    }
  });

  $("#tree").on("open_node.jstree", function (event, data) {
    $("#tree").jstree("delete_node", data.node.children);

    if (data.node.type === "folder") {
      $.get(`/processes?group=${data.node.text}`, function (json) {
        json
          .map((d) => {
            let leaf = {
              text: d.id,
              type: "file",
              children: [{}],
            };

            if (d.type == "group") {
              leaf.type = "folder";
            }

            return leaf;
          })
          .forEach((d) => {
            $("#tree").jstree("create_node", data.node.id, d);
          });
      });
    } else if (data.node.type === "file") {
      $.ajax({
        url: "/",
        type: "post",
        contentType: "application/javascript",
        data: "Classes.map(c=>c.name)",
        headers: {
          Process: getProcess(data.node),
        },
        success: function (json) {
          json
            .map((d) => {
              return {
                text: d,
                type: "class",
                children: [{}],
              };
            })
            .forEach((d) => {
              $("#tree").jstree("create_node", data.node.id, d);
            });
        },
      });
    } else if (data.node.type === "class") {
      $.ajax({
        url: "/",
        type: "post",
        contentType: "application/javascript",
        data: data.node.text + "s",
        headers: {
          Process: getProcess(data.node),
        },
        success: function (json) {
          json
            .map((d) => {
              return {
                text: d.id,
                type: "instance",
              };
            })
            .forEach((d) => {
              $("#tree").jstree("create_node", data.node.id, d);
            });
        },
      });
    }
  });

  $("#tree").on("close_node.jstree", function (event, data) {
    $("#tree").jstree("delete_node", data.node.children);
    $("#tree").jstree("create_node", data.node.id, {});
  });

  $.get("/processes", function (json) {
    json
      .map((d) => {
        let leaf = {
          text: d.id,
          type: "file",
          children: [{}],
        };

        if (d.type == "group") {
          leaf.type = "folder";
        }

        return leaf;
      })
      .forEach((d) => {
        $("#tree").jstree("create_node", "#", d);
      });
  });
});
