function displayGraph(node, json) {
  $("#result").empty();
  let container = document.getElementById("result");

  let objectType = ["class"];

  var graph = new mxGraph(container);

  var vertexStyle = graph.getStylesheet().getDefaultVertexStyle();
  vertexStyle[mxConstants.STYLE_ROUNDED] = true;
  vertexStyle[mxConstants.STYLE_FOLDABLE] = 0;
  vertexStyle[mxConstants.STYLE_STROKECOLOR] = "#c9c9c9";
  vertexStyle[mxConstants.STYLE_FILLCOLOR] = "#c9c9c9";
  vertexStyle[mxConstants.STYLE_RESIZABLE] = 0;
  vertexStyle[mxConstants.STYLE_FONTCOLOR] = "#323a40";

  var edgeStyle = graph.getStylesheet().getDefaultEdgeStyle();
  edgeStyle[mxConstants.STYLE_STROKECOLOR] = "#c9c9c9";
  edgeStyle[mxConstants.STYLE_FONTCOLOR] = "#323a40";

  var parent = graph.getDefaultParent();

  var layout = new mxFastOrganicLayout(graph);
  layout.minDistanceLimit = 0.5;

  graph.getModel().beginUpdate();

  let selected = graph.insertVertex(
    parent,
    null,
    node,
    0,
    0,
    250,
    200,
    "shape=swimlane;startSize=20;"
  );
  let count = 0;

  for (let prop in json) {
    if (typeof json[prop] === "object") {
      let obj = json[prop];

      if (objectType.includes(prop)) {
        let vertex = graph.insertVertex(
          parent,
          null,
          obj.key,
          0,
          0,
          250,
          200,
          "shape=swimlane;startSize=20;"
        );
        graph.insertEdge(parent, null, prop, selected, vertex);

        let count2 = 0;

        for (let p in obj) {
          if (typeof obj[p] !== "object") {
            graph.insertVertex(
              vertex,
              null,
              `${p}: ${obj[p]}`,
              0.5,
              0.2 + 0.1 * count2,
              0,
              0,
              null,
              true
            );

            count2++;
          }
        }
      } else {
        for (let p in obj) {
          let vertex = graph.insertVertex(
            parent,
            null,
            obj[p].key,
            0,
            0,
            250,
            200,
            "shape=swimlane;startSize=20;"
          );
          graph.insertEdge(parent, null, prop, selected, vertex);

          let count3 = 0;
          let obj2 = obj[p];

          for (let p2 in obj2) {
            if (typeof obj2[p2] !== "object") {
              graph.insertVertex(
                vertex,
                null,
                `${p2}: ${obj2[p2]}`,
                0.5,
                0.2 + 0.1 * count3,
                0,
                0,
                null,
                true
              );

              count3++;
            }
          }
        }
      }
    } else {
      graph.insertVertex(
        selected,
        null,
        `${prop}: ${json[prop]}`,
        0.5,
        0.2 + 0.1 * count,
        0,
        0,
        null,
        true
      );
      count++;
    }
  }

  layout.execute(parent);
  graph.getModel().endUpdate();
  graph.center();

  graph.addListener(mxEvent.CLICK, function (sender, event) {
    let properties = event.properties;
    if (properties.cell) {
      let cell = properties.cell;
      if (cell.parent.id === "1" && !cell.edge && properties.event.ctrlKey) {
        $.ajax({
          url: "/",
          type: "post",
          contentType: "application/javascript",
          data: `graph('${cell.value}')`,
          headers: {
            Process: "main",
          },
          success: function (json) {
            displayGraph(cell.value, json);
          },
        });
      }
    }
  });
}
