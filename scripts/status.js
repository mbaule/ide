$(function () {
  var cpuChart;
  var memoryChart;

  function fetchStatus() {
    $.ajax({
      url: "/",
      type: "post",
      contentType: "application/javascript",
      data:
        "let os=require('os');let status=new Object();status.load=os.loadavg();status.free=os.freemem();status.total=os.totalmem();status;",
      headers: {
        Process: "main",
      },
      success: function (status) {
        displayMemory(status);
        displayCPU(status);
      },
    });
  }

  function displayMemory(status) {
    let avaiable = Math.round(status.free / 1000000);
    let used = Math.round((status.total - status.free) / 1000000);

    memoryChart.data.datasets[0].data[0] = used;
    memoryChart.data.datasets[0].data[1] = avaiable;
    memoryChart.update();
  }

  function displayCPU(status) {
    let load = Math.round(status.load[0] * 100);
    let data = cpuChart.data.datasets[0].data;
    let labels = cpuChart.data.labels;

    let date = new Date();

    labels.shift();
    labels.push(`${date.getHours()}:${date.getMinutes()}`);
    data.shift();
    data.push(load);

    cpuChart.update();
  }

  let cpu = document.getElementById("cpu").getContext("2d");
  cpuChart = new Chart(cpu, {
    type: "line",
    data: {
      labels: ["", "", "", "", ""],
      datasets: [
        {
          data: [0, 0, 0, 0, 0],
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 99, 132, 0.2)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(255, 99, 132, 1)",
            "rgba(255, 99, 132, 1)",
            "rgba(255, 99, 132, 1)",
            "rgba(255, 99, 132, 1)",
          ],
          borderWidth: 0.5,
          pointRadius: 2,
        },
      ],
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              min: 0,
              max: 100,
              stepSize: 20,
            },
          },
        ],
      },
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: "CPU",
      },
    },
  });

  let memory = document.getElementById("memory").getContext("2d");
  memoryChart = new Chart(memory, {
    type: "doughnut",
    data: {
      labels: ["Used", "Available"],
      datasets: [
        {
          backgroundColor: [
            "rgba(64, 188, 216, 0.5)",
            "rgba(22, 219, 147, 0.5)",
          ],
          data: [0, 100],
          borderWidth: 0,
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: "Memory",
      },
    },
  });

  fetchStatus();
  setInterval(fetchStatus, 60000);
});
