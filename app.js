const express = require("express");
const app = express();

app.get("/", (req, res) => res.sendFile("nucleoid.html", { root: __dirname }));
app.get("*.*", (req, res) =>
  res.sendFile(req.url.slice(1), { root: __dirname })
);

app.listen(8080);
